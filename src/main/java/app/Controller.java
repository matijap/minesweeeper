package app;

import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.util.Map;


public class Controller {


    private void addEventListner(Cell[][] cells, Board board, Map<TextField, Cell> mappedTextFields, VBox vbox, TextField tf) {
        tf.setOnMouseClicked(mouseEvent -> {
            Cell cell = mappedTextFields.get(mouseEvent.getSource());
            if (cell.isCellVisible() == true)
                return;
            if (mouseEvent.getButton() == MouseButton.PRIMARY) {
                board.leftClick(cell, board.getBoard());
            }

            if (mouseEvent.getButton() == MouseButton.SECONDARY)
                board.rightClickflag(cell);
            vbox.getChildren().clear();
            vbox.getChildren().add(getGridPane(cells, mappedTextFields, board, vbox));
            System.out.println();
        });
    }


    private TextField getTextField(int row, int col, Cell[][] cells) {
        final Cell cell = cells[row][col];
        final TextField tf = new TextField();
        tf.setPrefHeight(50);
        tf.setPrefWidth(50);
        tf.setAlignment(Pos.CENTER);
        tf.setEditable(false);

        if (cell.isCellVisible()) {
            tf.setText(cells[row][col].getStatus().toString());
            tf.setStyle("-fx-control-inner-background: #0040ff;");
        }
        if (cell.isFlagged()) {
            tf.setStyle("-fx-control-inner-background: red;");
        }

        return tf;
    }

    public GridPane getGridPane(Cell[][] cells, Map<TextField, Cell> mappedTextFields, Board board, VBox vbox) {
        GridPane gridPane = new GridPane();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[0].length; j++) {
                final TextField tf = getTextField(i, j, cells);
                addEventListner(cells, board, mappedTextFields, vbox, tf);
                gridPane.setRowIndex(tf, i);
                mappedTextFields.put(tf, cells[i][j]);
                gridPane.setColumnIndex(tf, j);
                gridPane.getChildren().add(tf);


            }
        }
        return gridPane;
    }
}